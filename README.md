# HNLatLHCb

Sensitivity studies of HNL searches with inclusive decays at LHCb

## Setup in conda
* Setup `conda` with `ROOT` following [H.Schreiner's guide](https://iscinumpy.gitlab.io/post/root-conda/) or more simply, assuming homebrew is installed on your machine:
    * `brew cask install miniconda`
    * `echo "source /opt/miniconda3/etc/profile.d/conda.sh" > .conda/condasource.sh`
* Setup `conda` and install `rapidsim` from `conda-forge` in your a new enviroment:
 ```
source ~/.conda/condasource
conda config --add channels conda-forge
conda config --set channel_priority strict
conda create -n HNLatLHCb_00 python=3.7 rapidsim=1.5 rapidsim=1.5 matplotlib numpy tqdm future scipy uncertainties pyyaml root
 ```
* Install missing package from `conda-forge` once in your new enviroment:
```
conda activate HNLatLHCb_00
pip install particletools
```
* Initiate and download submodules:
  ```
  git submodule init
  git submodule update
  ```
* Compile cHNLdecay (depending on your `root` installation, `#include "TObjArray.h"` is possibly missing from `cHNLdecay.cxx`)
  ```
  cd cHNLdecay
  git checkout master
  make
  ```