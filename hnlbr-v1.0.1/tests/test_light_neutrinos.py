# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.realpath(os.path.join(os.path.dirname(
    os.path.realpath(__file__)), "..")))

def test_light_neutrinos(trace=False):
    """
    Compares the computed widths for h⁺ → l⁺ ν to the ones tabulated in the PDG,
    as a sanity check.
    """
    from hnlbr.data.particle import ParticleData
    pdata = ParticleData()
    from hnlbr.production.leptonic import ChargedLeptonicChannel
    import hnlbr.util.units as u
    import numpy as np
    mesons = [211, 321, 411, 431, 521, 541]
    # These mesons are considered stable by TDatabasePDG, so we need to enter
    # lifetimes manually.
    lifetimes = {
        211: 7.8045 * u.m(),
        321: 3.711 * u.m(),
        411: 311.8e-6 * u.m(),
        431: 149.9e-6 * u.m(),
        521: 491.1e-6 * u.m(),
        541: 0.507e-12 * u.s()
    }
    # Branching ratios to the various leptonic channels
    # Source: Review of Particle Physics (2016), Meson Summary Table
    leptons = [-11, -13, -15]
    one_hot = [
        [1., 0., 0.],
        [0., 1., 0.],
        [0., 0., 1.]
    ]
    brs = {
        # PDG ID: [(Br(e), δBr(e)), (Br(μ), δBr(μ)), (Br(τ), δBr(τ))]
        211: [(1.230e-4, 0.004e-4), (99.98770e-2, 0.00004e-2), (0., None)],
        321: [(1.582e-5, 0.007e-5), (63.56e-2, 0.11e-2), (0., None)],
        411: [(0., 8.8e-6), (3.74e-4, 0.17e-4), (0., 1.2e-3)],
        431: [(0., 8.3e-5), (5.56e-3, 0.25e-3), (5.55e-2, 0.24e-2)],
        521: [(0., 9.8e-7), (0., 1.0e-6), (1.09e-4, 0.24e-4)],
        541: [None, None, None] # Not listed in PDG
    }
    max_rel_err = 0.
    max_norm_err = 0.
    for meson in mesons:
        lifetime = lifetimes[meson]
        total_width = 1. / lifetime
        my_brs = brs[meson]
        for (br, lepton, couplings) in zip(my_brs, leptons, one_hot):
            if br is None:
                continue
            ch = ChargedLeptonicChannel(meson, lepton, pdata)
            pdg_br, pdg_err = br
            computed_width = ch.decay_rate(0., couplings)
            computed_br = computed_width / total_width
            if pdg_br > 0.:
                rel_err = abs(computed_br - pdg_br) / computed_br
                max_rel_err = max(max_rel_err, rel_err)
            if pdg_err is None:
                assert(computed_br == pdg_br)
                if trace:
                    print(u"PDG:      Br(" + str(ch) + ") = " + str(pdg_br) + " GeV")
            else:
                if trace:
                    print(u"PDG:      Br(" + str(ch) + ") = (" + str(pdg_br) + u" ± "
                          + str(pdg_err) + ") GeV")
                norm_err = abs(computed_br - pdg_br) / pdg_err
                max_norm_err = max(max_norm_err, norm_err)
            if trace:
                print(u"Computed: Br(" + str(ch) + ") = " + str(computed_br) + " GeV")
                if pdg_br > 0.:
                    print("Relative error:     " + str(rel_err))
                if pdg_err is not None:
                    print("Error / uncertainty:" + str(norm_err))
                print("--------------------------------------------------------------------------------")
    return max_rel_err, max_norm_err
