import numpy as np
from RunRapidSim import gen_and_sel

decay = 'B2muN'
ms = np.arange(0.5, 5.1, 0.1) #GeV
taus = np.array([1, 10, 100, 200, 500, 1000]) #ps
ngen = int(1.e5)

print('Generating {} in {} phase space points'.format(decay, len(ms)*len(taus)))
effs = np.zeros((len(taus), (len(ms))))

from tqdm import tqdm
progbar = tqdm(total=len(ms)*len(taus))
for ix, m in enumerate(ms):
    for iy, tau in enumerate(taus):
        eff = gen_and_sel(decay, m, tau, ngen)
        effs[iy, ix] = eff.nominal_value
        progbar.set_description('m={:.1f} GeV, tau={:.1f} ps, eff={}%'.format(m, tau, eff*100.))
        progbar.update(1)

# now let's plot the eff map
import matplotlib.pyplot as plt
import matplotlib.colors as colors

X, Y = np.meshgrid(ms, taus)
Z = effs*100.
fig, ax = plt.subplots(figsize=(7,4))
levs = np.linspace(0, np.max(Z), 13)
cf = ax.contourf(X,Y,Z, levs)
cbar = fig.colorbar(cf, ax=ax)
cbar.set_label(r'$\varepsilon ~(\%)$')
plt.xlabel(r'$m_N~[\rm{GeV}]$')
plt.ylabel(r'$\tau_N~[\rm{ps}]$')
plt.yscale('log')
plt.savefig('effmap_{}.pdf'.format(decay))

# assume that the search is background free -> upper limit is about 3.09 events (PDG)
L_7, L_8 = 1e15, 2e15 # 1/barn (run1)
xs4pi_7, xs4pi_8, xs4pi_14 = 0.25e-3, 0.29e-3, 0.53e-3 #barn 
fu, fd, fs, fc, fLb = 0.4, 0.4, 0.1, 0.002, 0.08
NBu = (L_7*xs4pi_7 + L_8*xs4pi_8)*fu*2 #factor 2 because two B per event
UL = 3.09 / effs / NBu

plt.clf()
for i, tau in enumerate(taus):
    plt.plot(X[i], UL[i], label=r'$\tau={}'.format(tau)+r'~\rm{ps}$')

# overlay corresponding limits from actual LHCb analysis
plt.gca().set_prop_cycle(None) #resetting color cycle
for tau in taus:
    d = np.genfromtxt('LHCb_B2muN/pimumu_{}ps.csv'.format(tau), delimiter=',')
    plt.plot(*list(zip(*d)), linestyle='dashed')

plt.yscale('log')
plt.ylim(2e-10, 2e-6)
plt.xlim(0.3, 5)
plt.xlabel(r'$m_N~[\rm{GeV}]$')
plt.ylabel(r'${\cal B}(B^+\to \mu^+N)\times {\cal B}(N\to\mu^+\pi^-)$')
plt.legend(loc='best')
plt.savefig('UpperLimits_1D.pdf'.format(decay))


