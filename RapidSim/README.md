# Minimal instructions
* Firstly, generate the decay config files:
    ```
    python generate_cfg_incldecay.py
    ```
* Run the phsp scan for exclusive B->muN with:
    ```
    python ScanPhSp.py ExclusiveAnalysis_Run1.yml
    python moneyplot.py
    ```
* Run the phsp scan for inclusive B decays with:
    ```
    python ScanPhSp.py InclusiveAnalysis_Run2.yml
    ```
* Plot the expected limits on the U2 vs mN plane with:
    ```
    python -i moneyplot.py
    ```

* Validating rapidsim approach
    ```
    python -i B2Kchi_scanPhSp.py
    python -i B2muN_scanPhSp.py
    ```
