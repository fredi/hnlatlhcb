import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('macosx')

#NevtsUL = 3.09
NevtsUL = 7 #FIXME

def plot_contour(X, Y, Z, color, linestyle, label, linewidth=1):
    plt.contour(X,Y,Z, [NevtsUL], linewidths=linewidth, colors=color, linestyles=linestyle)
    plt.plot([0,0], [1,2], color=color, linestyle=linestyle, linewidth=linewidth, label=label)

plt.figure(figsize=(8, 6))

ALL = np.transpose(np.loadtxt('./OtherLimits/ALL.csv', delimiter=','))
plt.fill_between(ALL[0], ALL[1], 1, color='grey', alpha=0.3, label='Previous limits')

DELPHI_prompt = np.transpose(np.loadtxt('./OtherLimits/DELPHI_prompt.csv', delimiter=','))
plt.plot(*DELPHI_prompt, label='DELPHI prompt')
DELPHI_displ = np.transpose(np.loadtxt('./OtherLimits/DELPHI_displ.csv', delimiter=','))
plt.plot(*DELPHI_displ, label='DELPHI displaced')

ATLAS_displ = np.transpose(np.loadtxt('./OtherLimits/ATLAS_1905.09787.csv', delimiter=','))
plt.plot(*ATLAS_displ, label='ATLAS displaced')

Belle = np.transpose(np.loadtxt('./OtherLimits/Belle1301.1105.csv', delimiter=','))
plt.plot(*Belle, label='Belle')

#NuTeV = np.transpose(np.loadtxt('./OtherLimits/NuTeV.csv', delimiter=','))
#plt.plot(*NuTeV, label='NuTeV')
#BEBC = np.transpose(np.loadtxt('./OtherLimits/BEBC.csv', delimiter=','))
#plt.plot(*BEBC, label='BEBC')
#NA3 = np.transpose(np.loadtxt('./OtherLimits/NA3.csv', delimiter=','))
#plt.plot(*NA3, label='NA3')

#BAU = np.transpose(np.loadtxt('./Baryogenesis_Umu_NaturalOrdering.csv', delimiter=','))
#plt.plot(*BAU, color='orange')
#BAUInv = np.transpose(np.loadtxt('./Baryogenesis_Umu_InvertedOrdering.csv', delimiter=','))
#plt.plot(*BAUInv, color='orange', linestyle='dashed')
#plt.fill_between(BAU[0], 1e-12, BAU[1], color='orange', alpha=0.1)

taus = np.load('./tauns.npy', allow_pickle=True)
X, Y, Z = taus[0], taus[1], taus[2]

c = 299.792458 # mm/ns
Zctau = Z*c
log10ctau = np.linspace(-3, 5, 9)
ctaulines = pow(10, log10ctau)
print(ctaulines)
ctaulabels = {1e-3: r'$c\tau= 1 \mu$m', 
             1e-2: r'$c\tau= 10 \mu$m', 
             1e-1: r'$c\tau=100 \mu$m', 
             1: r'$c\tau= 1$mm', 
             10: r'$c\tau=10$mm', 
             100: r'$c\tau = 100$mm', 
             1000: r'$c\tau = 1$m',
             1e4: r'$c\tau = 10$m',
             1e5: r'$c\tau = 100$m',
             }

CS = plt.contour(X, Y, Zctau, ctaulines, colors='grey', alpha=0.5, linewidths=0.5)
plt.clabel(CS, inline=True, fontsize=9, fmt=ctaulabels, use_clabeltext=True, inline_spacing=0)

#log10tau = np.linspace(-4, 1, 6)
#taulines = pow(10, log10tau)
#print(taulines)
#taulabels = {1e-4: r'$\tau=10^{-4}$ ns', 
             #1e-3: r'$\tau=10^{-3}$ ns', 
             #1e-2: r'$\tau=10^{-2}$ ns', 
             #1e-1: r'$\tau=0.1$ ns', 
             #1: r'$\tau = 1$ ns', 
             #10: r'$\tau = 10$ ns', 
             #100: r'$\tau=10^2$ ns', 
             #1000: r'$\tau=10^3$ ns'}

#CS = plt.contour(X, Y, Z, taulines, colors='grey', alpha=0.5, linewidths=0.5)
#plt.clabel(CS, inline=True, fontsize=9, fmt=taulabels, use_clabeltext=True, inline_spacing=0)








#revisedLHCb = np.transpose(np.loadtxt('./OtherLimits/revisedLHCb.csv', delimiter=','))
#plt.plot(*revisedLHCb, label='Run 1 exclusive (published)', color='red')




d=np.load('./nHNL_ExclusiveAnalysis_Run1.npy', allow_pickle=True)
X,Y,Z = d[0], d[1], d[2]
plot_contour(X, Y, Z, 'red', 'dashed', 'Run 1 exclusive (RapidSim)', linewidth=1)



d=np.load('./nHNL_InclusiveAnalysis.npy', allow_pickle=True)
X,Y,Z = d[0], d[1], d[2]
plot_contour(X, Y, Z, 'black', 'solid', 'Run 2 inclusive (RapidSim)', linewidth=2)

'''
ZBc = d[3]['[541, 0, -13]']
plot_contour(X, Y, ZBc, 'green', 'dotted', r'Inclusive $B_c\to\mu N$', linewidth=1)

ZBp = d[3]['[521, 0, -13]']
plot_contour(X, Y, ZBp, 'blue', 'dotted', r'Inclusive $B_u\to\mu N$', linewidth=1)

Z_b2c = 0*Z
for decay in d[3].keys():
    if ' 4' in decay or ' -4' in decay:
        Z_b2c += d[3][decay]
plot_contour(X, Y, Z_b2c, 'orange', 'dotted', r'Inclusive $b\to c\mu N$', linewidth=1)

Z_b2u = 0*Z
for decay in d[3].keys():
    if ' -21' in decay or ' 11' in decay:
        Z_b2u += d[3][decay]
plot_contour(X, Y, Z_b2u, 'violet', 'dotted', r'Inclusive $b\to u\mu N$', linewidth=1)
'''


plt.xlabel(r'$m_N~[\rm{GeV}]$')
plt.ylabel(r'$U^2_{\mu N}$')
plt.yscale('log')
plt.ylim(1e-8, 1e-2)
plt.xlim(1., 6)
plt.legend(loc='lower right')

#from labellines import labelLines
#labelLines(plt.gca().get_lines(), align=True, xvals=(1,5), zorder=2.5)
plt.ion()
plt.show()
plt.savefig('moneyplot.pdf')


