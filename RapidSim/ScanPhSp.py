import numpy as np
import sys
from RunRapidSim import gen_and_sel
from math import log10
from generate_cfg_incldecay import decayfilename
from uncertainties import ufloat

conffile = sys.argv[1]

import yaml
cfg = yaml.load(open(conffile, 'r'), Loader=yaml.FullLoader)

ms = np.linspace(cfg['mHNL']['min'], cfg['mHNL']['max'], cfg['mHNL']['nsteps'])
log10U2s = np.linspace(cfg['log10U2']['min'], cfg['log10U2']['max'], cfg['log10U2']['nsteps'])
U2s = pow(10, log10U2s)

from LHCb_collisions import totbb, fragfrac
from GetEffNormFromJpsiK import get_eff_factor
print('Getting efficiency scale factor based on JpsiK yield')
eff_factor = get_eff_factor(useKmumuPaperEff=cfg['useKmumuPaperEff'])

sys.path.insert(0, '../cHNLdecay')
from cpp_reader import get_prod_BR, get_decay_BR_lepton_meson, get_tau0ns

sys.path.insert(0, '../hnl_decays')
from hnl import HNL
import shipunit as u


print('Generating {} phase space points'.format(len(ms)*len(U2s)))
from tqdm import tqdm
progbar = tqdm(total=len(ms)*len(U2s)*len(cfg['gendecays']))

# initialise tables of yields (inclusive and exclusive)
nHNL = np.zeros((len(U2s), (len(ms))))
nHNL_excl = {}
for dcode in cfg['gendecays']:
    nHNL_excl[str(dcode)] = np.zeros((len(U2s), (len(ms))))

OSfactor = 2 if cfg['includeOS'] else 1

# now loop over masses and u2
for ix, m in enumerate(ms):
    for iy, u2 in enumerate(U2s):

        b = HNL(m, [0, u2, 0])
        BRpimu = b.findBranchingRatio('N -> pi+ mu-') * OSfactor
        tauns = b.computeNLifetime() / 1e-9

        Ntot = 0
        for dcode in cfg['gendecays']:
            decay = decayfilename(*dcode)
            BR_B2NX = get_prod_BR(m, tauns, abs(dcode[0]), abs(dcode[1]), abs(dcode[2]))

            if tauns>1:
                tausim = 1000 #ps
            else:
                tausim = 1000*tauns

            if BR_B2NX*BRpimu > 0.:
                eff = gen_and_sel(decay, m, tausim, int(cfg['nGen']), 
                        efftag=cfg['efftag']) * eff_factor 

                #assume that for tau>1ns eff scales linearly
                if tauns>1:
                    eff = eff * 1/tauns 

                N = totbb[cfg['dataset']] * 2*fragfrac[abs(dcode[0])] * BR_B2NX * BRpimu * eff

            else:
                N = ufloat(0, 0)

            nHNL_excl[str(dcode)][iy, ix] = N.n
            Ntot += N.n

            progbar.set_description(f'm={m:.1f}, log10(U2)={log10(u2):.2f}, nHNL={Ntot:.2f}, added {decay}')
            progbar.update(1)

        nHNL[iy, ix] = Ntot


X, Y = np.meshgrid(ms, U2s)
print('nHNL = ', nHNL)
print('nHNL = ', nHNL_excl)
outfile = 'nHNL_{}.npy'.format(conffile.replace('.yml', ''))
np.save(outfile, (X, Y, nHNL, nHNL_excl))
print(f'wrote number of selected HNL to {outfile}')



