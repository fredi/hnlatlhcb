import numpy as np
from RunRapidSim import gen_and_sel

decay = 'B2Kchi'
ms = np.arange(1, 4.8, 0.1) #GeV
log10taus = np.linspace(0, 3, 8)
taus = pow(10, log10taus) #ps
ngen = int(5.e4)

print('Generating {} in {} phase space points'.format(decay, len(ms)*len(taus)))
effs = np.zeros((len(taus), (len(ms))))

from tqdm import tqdm
progbar = tqdm(total=len(ms)*len(taus))
for ix, m in enumerate(ms):
    for iy, tau in enumerate(taus):
        eff = gen_and_sel(decay, m, tau, ngen)
        effs[iy, ix] = eff.nominal_value
        progbar.set_description('m={:.1f} GeV, tau={:.1f} ps, eff={}%'.format(m, tau, eff*100.))
        progbar.update(1)

# now let's plot the eff map
import matplotlib.pyplot as plt
import matplotlib.colors as colors

X, Y = np.meshgrid(ms, taus)
Z = effs*100. # in percentage
fig, ax = plt.subplots(figsize=(7,4))
levs = np.linspace(0, 2.4, 13)
cf = ax.contourf(X,Y,Z, levs)
cbar = fig.colorbar(cf, ax=ax)
cbar.set_label(r'$\varepsilon ~(\%)$')
plt.xlabel(r'$m_\chi~[\rm{GeV}]$')
plt.ylabel(r'$\tau_\chi~[\rm{ps}]$')
plt.yscale('log')
plt.savefig('effmap_{}.pdf'.format(decay))

# assume that the search is background free -> upper limit is about 3.09 events (PDG)
L_7, L_8 = 1e15, 2e15 # 1/barn (run1)
xs4pi_7, xs4pi_8, xs4pi_14 = 0.25e-3, 0.29e-3, 0.53e-3 #barn 
fu, fd, fs, fc, fLb = 0.4, 0.4, 0.1, 0.002, 0.08
NBu = (L_7*xs4pi_7 + L_8*xs4pi_8)*fu*2 #factor 2 because two B per event
UL = 3.09 / effs / NBu

plt.clf()
fig, ax = plt.subplots(figsize=(7,4))
lev_exp = np.arange(-10, -7.1, 0.25)
levs = np.power(10, lev_exp)
cf = ax.contourf(X,Y,UL, levs, norm=colors.LogNorm())
cbar = fig.colorbar(cf, ax=ax, ticks=[1e-10, 1e-9, 1e-8, 1e-7])
cbar.set_label(r'${\cal B}(B^+\to K^+\chi)\times {\cal B}(\chi\to\mu\mu)$')
plt.xlabel(r'$m_\chi~[\rm{GeV}]$')
plt.ylabel(r'$\tau_\chi~[\rm{ps}]$')
plt.yscale('log')
plt.savefig('UpperLimits_{}.pdf'.format(decay))



